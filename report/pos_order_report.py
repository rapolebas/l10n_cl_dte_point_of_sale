# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools


class PosOrderReport(models.Model):
    _inherit = "report.pos.order"

    document_class_id = fields.Many2one('sii.document_class', string='Tipo de Documento SII', readonly=True)


    def _select(self):
        select = super(PosOrderReport, self)._select()
        select += """,
            s.document_class_id as document_class_id
        """
        return select

    def _group_by(self):
        group = super(PosOrderReport, self)._group_by()
        group += """,
            s.document_class_id
        """
        return group
